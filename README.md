# Mapathon Activities (Themes)
- Map Your Home
- Map Your School
- Map Your Local Church/Temple/Mosque
- Map Your Junctions
- Map Your Local Pond/ Lake
- Map Your Local Public Garden / Park
- Old Import Tidying (see for example the Old Aerodromes challenge on MapRoulette)
- Map a challenged part of town
- Invite local government folks to talk about GIS data and tell them about OSM.
- Bus Routes and Bus Stops
- Add bus / rail route relations

## What do I need to know to host an event?
- The rule is simple: anything you commit to OpenStreetMap, should contain the hashtag chosen (eg: #mapathon) in the comment
- Your event can be an outside surveying event or an inside editing event
- Design your event friendly to newcomers, make sure there's someone there to explain to first timers the fundamentals of OpenStreetMap and show them the first steps of editing and surveying.
- The location should have fast internet and a place to sit for everyone. Remind attendees to bring a computer, mouse, and any other devices you'll need for your event.
- Collect name and email from attendees, keep them to yourself. This information is to send on a feedback survey and only for that.
